Linear Algebra toolbox

Purpose
-------

The goal of this toolbox is to provide a collection of
algorithms for linear algebra.

These algorithms are most of the time already provided by Scilab, but
are available here for comparison purpose.

The linalg_pow function is much faster than Scilab's pow,
when the exponent is large.
The algorithm used here is based on binary exponentiation, and
requires only log2(p) iterations, compared to p iterations for the
naive pow.
For example, for p=99999, it was found that linalg_pow requires 0.003
seconds for a 5-by-5 matrix, while Scilab requires 2.7 seconds.

The linalg_zhbev function provides the same feature as Lapack's
ZHBEV, that is, computes the eigenvalues of a complex hermitian band matrix.
The other method in Scilab to compute eigenvalues of sparse matrices
is to use Arnoldi's iterations.
This does not make use of the Hermitian structure of some matrices.
This is why the linalg_zhbev is much faster than Scilab's routines for
Hermitian band matrices.

The solution of A*X=B in Scilab is based on the backslash operator.
The backslash operator is switching from Gaussian Elimination (with
pivoting) to Linear Least Squares when the condition number of
the matrix is larger than roughly 10^8.
This switch is annoying for matrices which condition number are
between 10^8 and 10^16, where the Gaussian Elimination can
produce a more accurate result than Least Squares.
The linalg_gesv function provided in this module is a direct
interface to the Gaussian Elimination from Lapack.
In some cases, this function can produce significantly more accurate
solution than backslash.

Features :
-------

 * linalg_chol — Computes the Cholesky decomposition.
 * linalg_condeig — Computes the condition number of the eigenvalues of a matrix.
 * linalg_expm — Computes the exponential of a matrix.
 * linalg_factorlu — Computes the LU decomposition without pivoting.
 * linalg_factorlupivot — Computes the LU decomposition with pivoting.
 * linalg_gaussnaive — Solves a linear equation by Gauss method and no pivoting.
 * linalg_gausspivotal — Computes the solution of a linear equation with Gauss and row pivoting.
 * linalg_gausspivotalnaive — Computes the solution of a linear equation with Gauss and row pivoting.
 * linalg_hbandL — Converts a complex hermitian band matrix into its compact form.
 * linalg_pow — Computes A^p
 * linalg_powfast — Computes A^p
 * linalg_rayleighiteration — Computes approximated a pair of eigenvalue and eigenvector.
 * linalg_solvelu — Computes the solution of a linear equation, given its LU decomposition.

BLAS/LAPACK
 * linalg_dgemm — Computes D = Alpha*A*B+ Beta*C for a real matrix.
 * linalg_dgesv — Solves A*X = B for real equations.
 * linalg_dsyev — Computes the eigenvalues of a symmetric real matrix.
 * linalg_gemm — Computes D = Alpha*A*B+ Beta*C for a real or complex matrix.
 * linalg_gesv — Computes the real or complex solution X of A*X=B.
 * linalg_zgemm — Computes D = Alpha*A*B+ Beta*C for a complex matrix.
 * linalg_zgesv — Solves A*X = B for complex equations.
 * linalg_zhbev — Computes the eigenvalues of a complex hermitian band matrix.

Dependencies
------------

 * This module depends on the assert module.
 * This module depends on the apifun module (v0.2).
 * This module depends on the helptbx module (only to update the help).

Forge
-------

http://forge.scilab.org/index.php/p/linalg/

Authors
-------

 * Copyright (C) 2008 - 2010 - Michael Baudin
 * Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
 * Copyright (C) 2010 - DIGITEO - Allan Cornet
 * Copyright (C) INRIA
 * Copyright (C) 2009 - Vladimir Sergievskiy
 * Copyright (C) 2010 - Sylvan Brocard

Bibliography
-------

"Matrix Computations", Golub and Van Loan, Third Edition, The Johns Hopkins University Press
http://www.math.siu.edu/matlab/tutorials.html
"The threshold level for conditioning in backslash is too small", Michael Baudin, http://bugzilla.scilab.org/show_bug.cgi?id=9196
"The help page of backslash does not print the singularity level", Michael Baudin, http://bugzilla.scilab.org/show_bug.cgi?id=7497

Licence
-------

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

TODO
 * add the expm2 function from http://atoms.scilab.org/toolboxes/DeCaToKi.
 * Add a function to solve triangular systems y = T\x.
   Backslash does not use the triangular structure of T.
   See the message :
   http://www.les-mathematiques.net/phorum/read.php?15,698295

