// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = linalg_gaussnaive(varargin)
    // Solves a linear equation by Gauss method and no pivoting.
    //
    // Calling Sequence
    //   x = linalg_gaussnaive(A, b)
    //   x = linalg_gaussnaive(A, b, verbose)
    //
    // Parameters
    // A : a n-by-n matrix of doubles
    // b : a n-by-1 matrix of doubles
    // verbose : a 1-by-1 matrix of boolean (default verbose = %f),
    //           set to true to display intermediate messages
    // x : a n-by-1 matrix of doubles
    //
    // Description
    //   Returns the solution of Ax = b
    //   with a Gauss elimination and no pivoting.
    //   This algorithm might be inaccurate if A near singular.
    //   There is no solution is A is singular.
    //   Moreover, it might inaccurate even if A is correctly conditionned.
    //   Uses a naive algorithm with all loops expanded (no vectorization).
    //
    // Examples
    // A = [3  17   10
    //      2  4   -2
    //      6  18  -12];
    // b = [67
    //      4
    //      6];
    // x = linalg_gaussnaive(A, b);
    // xe = [1
    //       2
    //       3];
    // // See what happens
    // x = linalg_gaussnaive(A, b, %t);
    //
    // // A failure case : pivoting is necessary
    // A = [2*%eps  1
    //      1       1];
    // xe = [1/10
    //       1/3];
    // b = A * xe;
    // x = linalg_gaussnaive(A, b);
    //
    // // See the algorithm
    // edit linalg_gaussnaive
    //
    // Authors
    // Copyright (C) 2008 - 2010 - Michael Baudin
    // Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_gaussnaive", rhs, 2:3);
    apifun_checklhs("linalg_gaussnaive", lhs, 0:1);

    // Get arguments
    A  = varargin(1);
    b  = varargin(2);
    verbose = apifun_argindefault(varargin, 3, %f);

    // Check types
    apifun_checktype("linalg_gaussnaive", A, "A", 1, "constant");
    apifun_checktype("linalg_gaussnaive", b, "b", 2, "constant");
    apifun_checktype("linalg_gaussnaive", verbose, "verbose", 3, "boolean");

    // Check size
    [nrows ncols] = size(A);
    apifun_checksquare("linalg_gaussnaive", A, "A", 1);
    apifun_checkvector("linalg_gaussnaive", b, "b", 2);
    apifun_checkdims  ("linalg_gaussnaive", b, "b", 2, [nrows 1]);
    apifun_checkscalar("linalg_gaussnaive", verbose, "verbose", 3);

    // Proceed...
    n = size(A, "r");

    // Perform Gauss transforms
    for k = 1:n-1
        // Perform transform for lines k+1 to n
        for i = k+1:n
            A(i, k) = A(i, k) / A(k, k);
        end
        for i = k+1:n
            for j = k+1:n
                A(i, j) = A(i,j) ..
                       - A(i,k) * A(k,j);
            end
        end
        if verbose then
            printf("Step #%d / %d\n", k, n);
            printf("A:");
            disp(A);
        end
    end

    // Perform forward substitution
    for k = 1:n-1
        // Substitution
        for i = k+1:n
            b(i) = b(i) - b(k) * A(i, k);
        end
    end
    if verbose then
        printf("After forward loop, b:\n");
        disp(b);
    end

    // Perform backward substitution
    for k = n:-1:1
        t = 0;
        for j = k+1:n
            t = t + A(k, j) * b(j);
        end
        b(k) = (b(k) - t) / A(k, k);
    end
    if verbose then
        printf("After backward loop, b:\n");
        disp(b);
    end
    x = b;
endfunction

