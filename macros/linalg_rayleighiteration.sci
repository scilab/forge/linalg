// Copyright (C) 2010 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [mu, b, iter] = linalg_rayleighiteration(varargin)
    // Computes approximated a pair of eigenvalue and eigenvector.
    //
    // Calling Sequence
    //   mu = linalg_rayleighiteration(A, mu0, b0)
    //   mu = linalg_rayleighiteration(A, mu0, b0, rtol)
    //   mu = linalg_rayleighiteration(A, mu0, b0, rtol, itmax)
    //   mu = linalg_rayleighiteration(A, mu0, b0, rtol, itmax, verbose)
    //   [mu, b] = linalg_rayleighiteration(...)
    //   [mu, b, iter] = linalg_rayleighiteration(...)
    //
    // Parameters
    //   A : a n-by-n matrix of doubles, real, symetric
    //   mu0 : a 1-by-1 matrix of doubles, an estimated eigenvalue
    //   b0 : a n-by-1 matrix of doubles, an estimated eigenvector
    //   rtol : a 1-by-1 matrix of doubles(default rtol = %eps),
    //          a relative tolerance
    //   itmax : a 1-by-1 matrix of doubles, integer value
    //           (default itmax = 100), a maximum number of iterations
    //   verbose : a 1-by-1 matrix of booleans (default verbose = %f),
    //             set to true to print convergence messages
    //   mu : a 1-by-1 matrix of doubles, an approximated eigenvalue
    //   b : a n-by-1 matrix of doubles, an approximated eigenvector
    //   iter : the number of iterations performed
    //
    // Description
    //   Uses Rayleigh iterations to find an approximate
    //   eigenvalue and an approximate eigenvector.
    //
    //   The algorithm stops if norm(A*b-mu*b) < rtol*norm(mu*b).
    //
    // Examples
    // // Example #1
    // A = [6 2 3
    //      2 5 1
    //      3 1 3];
    // b = [1
    //      1
    //      1];
    // mu = 1;
    // [mu b iter] = linalg_rayleighiteration(A, mu, b)
    //
    // // Configure options
    // rtol = 1.e-4;
    // itmax = 20;
    // verbose = %f;
    // [mu b iter] = linalg_rayleighiteration(A, mu, b, rtol, itmax, verbose)
    //
    // // See what happens
    // [mu b iter] = linalg_rayleighiteration(A, mu, b, rtol, itmax, %t)
    //
    // // Example #2
    // A = [1 2 3
    //      1 2 1
    //      3 2 1];
    // b = [1
    //      1
    //      1];
    // mu = 200;
    // [mu b iter] = linalg_rayleighiteration(A, mu, b)
    //
    // // Example #3
    // // The initial eigenvalue is close to the
    // // exact one.
    // //
    // A = [1 2 3
    //      1 2 1
    //      3 2 1];
    // b = [1
    //      1
    //      1];
    // mu = 0.7639320225002093067701;
    // [mu b iter] = linalg_rayleighiteration(A, mu, b)
    //
    // Authors
    // Copyright (C) 2010 - 2011 - Michael Baudin
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_rayleighiteration", rhs, 3:6);
    apifun_checklhs("linalg_rayleighiteration", lhs, 0:3);

    // Get arguments
    A = varargin(1);
    mu = varargin(2);
    b = varargin(3);
    rtol = apifun_argindefault(varargin, 4, %eps);
    itmax = apifun_argindefault(varargin, 5, 100);
    verbose = apifun_argindefault(varargin, 6, %f);

    // A, mu, b, rtol, itmax, verbose
    // Check type
    apifun_checktype("linalg_rayleighiteration", A, "A", 1, "constant");
    apifun_checktype("linalg_rayleighiteration", mu, "mu", 2, "constant");
    apifun_checktype("linalg_rayleighiteration", b, "b", 3, "constant");
    apifun_checktype("linalg_rayleighiteration", rtol, "rtol", 4, "constant");
    apifun_checktype("linalg_rayleighiteration", ..
                     itmax, "itmax", 5, "constant");
    apifun_checktype("linalg_rayleighiteration", ..
                     verbose, "verbose", 6, "boolean");

    // Check size
    apifun_checkscalar("linalg_rayleighiteration", mu, "mu", 2);
    [nrows, ncols] = size(A);
    apifun_checkveccol("linalg_rayleighiteration", b, "b", 3, nrows);
    apifun_checkscalar("linalg_rayleighiteration", rtol, "rtol", 4);
    apifun_checkscalar("linalg_rayleighiteration", itmax, "itmax", 5);
    apifun_checkscalar("linalg_rayleighiteration", verbose, "verbose", 6);

    // Check content
    apifun_checkflint("linalg_rayleighiteration", itmax, "itmax", 5);
    apifun_checkgreq("linalg_rayleighiteration", itmax, "itmax", 5, 1);

    // Proceed...

    I = eye(A);
    iter = 1;
    bprev = b;
    Ab = A * b;
    for iter = 1:itmax
        if norm(Ab - mu*b) < rtol * norm(mu*b) then
            break
        end
        C = A - mu*I;
        //x = C\b
        //x = linalg_gausspivotal(C, b, %f)
        x = linalg_gesv(C, b);
        bprev = b;
        b = x / norm(x);
        Ab = A * b;
        mu = b' * Ab / (b'*b);
        if verbose then
            mprintf("iter = %d, mu = %s, b = %s\n", ..
                    iter, string(mu), strcat(string(b), " "));
        end
    end

    // Make sure that the first component of b is positive
    if b(1) < 0 then
        b = -b;
    end
endfunction

