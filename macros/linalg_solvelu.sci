// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function x = linalg_solvelu(varargin)
    // Computes the solution of a linear equation, given its LU decomposition.
    //
    // Calling Sequence
    //   x = linalg_solvelu(L, U, b)
    //   x = linalg_solvelu(L, U, b, verbose)
    //
    // Parameters
    // L : a n-by-n matrix of doubles, lower triangular.
    // U : a n-by-n matrix of doubles, upper triangular.
    // b : a n-by-1 matrix of doubles
    // verbose : a 1-by-1 matrix of boolean (default verbose = %f),
    //           set to %t to display intermediate messages
    // x : a n-by-1 matrix of doubles
    //
    // Description
    //   Solve Ax = b with A = L*U and
    //   L lower triangular and U upper triangular.
    //   Effective algorithm with vectorization.
    //
    // Examples
    // L = [1 0 0 0 0
    //      1 1 0 0 0
    //      1 1 1 0 0
    //      1 1 1 1 0
    //      1 1 1 1 1];
    // U = [1 1 1 1 1
    //      0 1 1 1 1
    //      0 0 1 1 1
    //      0 0 0 1 1
    //      0 0 0 0 1];
    // b = [5
    //      9
    //      12
    //      14
    //      15];
    // xe = [1
    //       1
    //       1
    //       1
    //       1];
    // x = linalg_solvelu(L, U, b)
    // // See what happens
    // x = linalg_solvelu(L, U, b, %t)
    //
    // // See the algorithm
    // edit linalg_solvelu
    //
    // Authors
    // Copyright (C) 2008 - 2010 - Michael Baudin
    // Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_solvelu", rhs, 3:4);
    apifun_checklhs("linalg_solvelu", lhs, 0:1);

    // Get arguments
    L = varargin(1);
    U = varargin(2);
    b = varargin(3);
    verbose = apifun_argindefault(varargin, 4, %f);

    // Check types
    apifun_checktype("linalg_solvelu", L, "L", 1, "constant");
    apifun_checktype("linalg_solvelu", U, "U", 2, "constant");
    apifun_checktype("linalg_solvelu", b, "b", 3, "constant");
    apifun_checktype("linalg_solvelu", verbose, "verbose", 4, "boolean");

    // Check size
    [nrows ncols] = size(L);
    apifun_checksquare("linalg_solvelu", L, "L", 1);
    apifun_checksquare("linalg_solvelu", U, "U", 2);
    apifun_checkvector("linalg_solvelu", b, "b", 3);
    apifun_checkdims("linalg_solvelu", U, "U", 2, [nrows ncols]);
    apifun_checkdims("linalg_solvelu", b, "b", 3, [nrows 1]);
    apifun_checkscalar("linalg_solvelu", verbose, "verbose", 4);

    // Proceed...
    n = size(L, "r");
    y = zeros(n, 1);
    // Forward loop
    if n > 0
		y(1) = b(1) / L(1, 1);
		for i = 2:n
			y(i) = (b(i) - L(i, 1:i-1) * y(1:i-1)) / L(i, i);
		end
    end
	if verbose then
        printf("After forward loop, y:\n");
        disp(y);
    end
    // Backward loop
    x = zeros(n, 1);
	if n > 0 then
		x(n) = y(n) / U(n, n);
		for i = n-1:-1:1
			x(i) = (y(i) - U(i, i+1:n) * x(i+1:n)) / U(i, i);
		end
	end
    if verbose then
        printf("After backward loop, x:\n");
        disp(x);
    end
endfunction

