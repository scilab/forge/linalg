// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - Sylvan Brocard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function AHLband = linalg_hbandL(Afull, nsubdiag)
    // Converts a complex hermitian band matrix into its compact form.
    //
    // Calling Sequence
    //   AHLband = linalg_hbandL(Afull, nsubdiag)
    //
    // Parameters
    // Afull : a n-by-n matrix of complex doubles
    // nsubdiag : a 1-by-1 matrix of doubles, integer value, positive,
    //            the number of non-null subdiagonals of Afull
    // AHLband : a nsubdiag+1-by-n matrix of doubles, the Hermitian
    //           Band Lower storage format of Afull.
    //
    // Description
    // Transforms a complex Hermitian Band matrix into its compact form
    // suitable for the zhbev algorithm.
    // We store in AB only the lower triangular part of the full matrix A.
    //
    // Examples
    // Afull = [3       1+7*%i   0
    //          1-7*%i  4       -2-8*%i
    //          0      -2+8*%i -12];
    // nsubdiag = 1;
    // AHLband = linalg_hbandL(Afull, nsubdiag)
    // expected = [3       4      -12
    //             1-%i*7 -2+%i*8   0];
    //
    // // Combine hbandL and zhbev
    // // Create a n-by-n Hermitian matrix.
    // n = 50;
    // nsubdiag = 5;
    // Ar = zeros(n, n);
    // for i = 1:n
    //   j = [i:i+nsubdiag-1];
    //   j = max(min(j, n), 1);
    //   Ar(i, j) = j;
    // end
    // Ai = zeros(n, n);
    // for i = 1:n
    //   j = [i:i+nsubdiag-1];
    //   j = max(min(j, n), 1);
    //   Ai(i, j) = j;
    // end
    // Afull = Ar+%i*Ai + Ar'-%i*Ai';
    // // Convert it into band-form
    // AHLband = linalg_hbandL(Afull, nsubdiag);
    // // Use zhbev
    // D = linalg_zhbev(AHLband);
    // D = gsort(D, "g", "i");
    // // Compare with spec
    // Dfull = spec(Afull);
    // Dfull = gsort(Dfull, "g", "i");
    // // Display relative error : rtol should be from 10 to 1000
    // rtol = max(abs(D-Dfull) ./ abs(D)/%eps)
    //
    // Authors
    // Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
    // Copyright (C) 2010 - Sylvan Brocard

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_hbandL", rhs, 2:2);
    apifun_checklhs("linalg_hbandL", lhs, 0:1);

    // Check type
    apifun_checktype("linalg_hbandL", Afull, "Afull", 1, "constant");
    apifun_checktype("linalg_hbandL", nsubdiag, "nsubdiag", 2, "constant");

    // Check size
    apifun_checkscalar("linalg_hbandL", nsubdiag, "nsubdiag", 2);

    // Check content
    apifun_checkflint("linalg_hbandL", nsubdiag, "nsubdiag", 2);
    apifun_checkgreq("linalg_hbandL", nsubdiag, "nsubdiag", 2, 1);

    // Proceed...

    [nmax mmax] = size(Afull);
    if nmax <> mmax then
        localstr = gettext("%s: Wrong input argument #%d. " ..
                         + "Expected a square matrix, " ..
                         + "but found a %d-by-%d matrix.");
        msg = msprintf(localstr, "linalg_hbandL", 1, nmax, mmax);
        error(msg);
    end
    if isreal(Afull) then
        Afull = complex(Afull, 0);
    end
    AHLband = zeros(nsubdiag+1, nmax);
    for j = 1:nmax
        m = min(nmax, j+nsubdiag);
        i = [j:m];
        AHLband(1+i-j, j) = Afull(i, j);
    end
endfunction

