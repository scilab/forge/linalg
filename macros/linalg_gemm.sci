// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function D = linalg_gemm(Alpha, A, B, Beta, C)
    // Computes D = Alpha*A*B + Beta*C for a real or complex matrix.
    //
    // Calling Sequence
    //   D = linalg_gemm(Alpha, A, B, Beta, C)
    //
    // Parameters
    // Alpha : a 1-by-1 matrix of doubles (real or complex)
    // A : a m-by-n matrix of doubles (real or complex)
    // B : a n-by-p matrix of doubles (real or complex)
    // Beta : a 1-by-1 matrix of doubles (real or complex)
    // C : a m-by-p matrix of doubles (real or complex)
    // D : a m-by-p matrix of doubles (real or complex), D = Alpha*A*B + Beta*C
    //
    // Description
    //   Computes D = Alpha*A*B + Beta*C for a real or complex matrix.
    //   Uses BLAS/DGEMM if all inputs are real,
    //   uses ZGEMM if any input is complex.
    //   Convert to complex matrices when necessary to call ZGEMM.
    //
    // Examples
    // // A real matrix
    // Alpha = 0.1234;
    // Beta = 5.678;
    // m = 3;
    // n = 4;
    // k = 2;
    // C = matrix(1:m*n, m, n);
    // A = matrix(1:m*k, m, k);
    // B = matrix(1:k*n, k, n);
    // D = linalg_gemm(Alpha, A, B, Beta, C)
    //
    // // See with matrices of mixed types
    // Alpha = 1 + %i;
    // Beta = Alpha;
    // A = matrix(1:4, 2, 2);
    // B = A;
    // C = A;
    // D = linalg_gemm(Alpha, A, B, Beta, C)
    //
    // Authors
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    function B = linalg_complexify(A)
        if isreal(A) then
            B = complex(A);
        else
            B = A;
        end
    endfunction

    [lhs rhs] = argn();
    apifun_checkrhs("linalg_gemm", rhs, 5:5);
    apifun_checklhs("linalg_gemm", lhs, 0:1);

    // Check type
    apifun_checktype("linalg_gemm", Alpha, "Alpha", 1, "constant");
    apifun_checktype("linalg_gemm", A,     "A",     2, "constant");
    apifun_checktype("linalg_gemm", B,     "B",     3, "constant");
    apifun_checktype("linalg_gemm", Beta,  "Beta",  4, "constant");
    apifun_checktype("linalg_gemm", C,     "C",     5, "constant");

    // Check size
    apifun_checkscalar("linalg_gemm", Alpha, "Alpha", 1);
    [mA nA] = size(A);
    [mB nB] = size(B);
    [mC nC] = size(C);
    apifun_checkdims  ("linalg_gemm", A,    "A",    3, [mA, mB]);
    apifun_checkdims  ("linalg_gemm", B,    "B",    3, [nA, nB]);
    apifun_checkscalar("linalg_gemm", Beta, "Beta", 4);
    apifun_checkdims  ("linalg_gemm", C,    "C",    5, [mA, nB]);

    // Switch to DGEMM/ZGEMM
    isRealAlpha = isreal(Alpha);
    isRealA     = isreal(A);
    isRealB     = isreal(B);
    isRealBeta  = isreal(Beta);
    isRealC     = isreal(C);
    if isRealAlpha & isRealA & isRealB ..
     & isRealBeta  & isRealC then
        D = linalg_dgemm(Alpha, A, B, Beta, C);
    else
        Alpha = linalg_complexify(Alpha);
        A     = linalg_complexify(A);
        B     = linalg_complexify(B);
        Beta  = linalg_complexify(Beta);
        C     = linalg_complexify(C);
        D = linalg_zgemm(Alpha, A, B, Beta, C);
    end
endfunction

