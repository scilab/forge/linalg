changelog of the Linear Algebra Scilab Toolbox

0.4.1
    * Updated for Scilab 2023
0.4 
    * Updated for Scilab 6.
    
0.3.1
    * Updated to Scilab 5.4.

0.3
    * Updated factorlu to improve portability.

0.2
    * Added interface to dgesv.
    * Added interface to zgesv.
    * Added generic linalg_gemm.
    * Added generic linalg_gesv.
    * Used apifun to check input arguments.
    * Provide optional verbose option for linalg_factorlu.
    * Provide optional verbose option for linalg_solvelu.
    * Provide optional verbose option for linalg_factorlupivot.
    * Provide optional verbose option for linalg_gaussnaive.
    * Provide optional verbose option for linalg_rayleighiteration.
    * Fixed problems in linalg_rayleighiteration caused by the switch
      to linear least squares in backslash.
      The algorithm had problems when it came closer to
      the exact result, because an intermediate matrix
      becomes more and more ill-conditionned.
      In this case, Scilab switches the backslash operator
      from Gaussian elimination to least squares, which
      makes the algorithms going nuts.
      Hence, we cannot use this algorithm with a relative
      tolerance which is too small.
      We now use linalg_gesv and the problem is gone away.
    * Reduced the default relative tolerance in linalg_rayleighiteration
      to %eps.

0.1
    * Initial version (January 2010)
    * Created help pages for macros.
    * Copied gateways from Scilab sources, created help pages, unit tests.
    * Merged zhbev from Sylvan Brocard.
    * Added a draft of fast pow.
    * Created a separated matrix exponential.
    * Updated help pages.
    * Removed unnecessary demos.
    * Added separate Cholesky decomposition.
    * Added separate Rayleigh iteration.
    * Added help page for compiled matrix pow.
    * Added separate macro for matrix pow.
    * Renamed fastpow to powfast, for improved order in help pages.
    * Updated linalg_dgemm to new API.
    * Updated linalg_zgemm to new API.
    * Updated linalg_dsyev to new API.




