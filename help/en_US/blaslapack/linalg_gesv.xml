<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from linalg_gesv.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="linalg_gesv" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>linalg_gesv</refname><refpurpose>Computes the real or complex solution X of A*X=B.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   X = linalg_gesv ( A , B )

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>A :</term>
      <listitem><para> a n-by-n matrix of doubles (real or complex)</para></listitem></varlistentry>
   <varlistentry><term>B :</term>
      <listitem><para> a n-by-p matrix of doubles (real or complex)</para></listitem></varlistentry>
   <varlistentry><term>X :</term>
      <listitem><para> a n-by-p matrix of doubles (real or complex), the solution of A*X = B.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Computes the real or complex solution X of A*X=B.
Uses BLAS/DGESV if all inputs are real, uses ZGESV if any input is complex.
Convert to complex matrices when necessary to call ZGESV.
   </para>
   <para>
The solution of A*X=B in Scilab is based on the backslash operator.
The backslash operator is switching from Gaussian Elimination (with
pivoting) to Linear Least Squares when the condition number of
the matrix is larger than roughly 10^8.
This switch is annoying for matrices which condition number are
between 10^8 and 10^16, where the Gaussian Elimination can
produce a more accurate result than Least Squares.
The linalg_gesv function provided in this module is a direct
interface to the Gaussian Elimination from Lapack.
In some cases, this function can produce significantly more accurate
solution than backslash.
See the example below for the Hilbert matrix of size 8.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// A real system of equations
A=  [
1 2
3 4
];
e = [5,1,2,3;6,2,3,4];
b = [17,5,8,11;39,11,18,25];
x = linalg_gesv(A,b)
// A mixed system
A=  [
1 2
3 4
] + %i* [
5 6
7 8];
e = [5;6];
b = [17;39] + %i * [61;83];
x = linalg_gesv(A,b)

// Compare with Scilab for difficult matrix
n = 8;
e = ones(n,1);
A = testmatrix("hilb",n);
b = A*e;
// With backslash
x = A\b;
norm(A*x-b)/norm(b)
norm(x-e)/norm(e)
// With gesv
x = linalg_gesv(A,b);
norm(A*x-b)/norm(b)
norm(x-e)/norm(e)

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
