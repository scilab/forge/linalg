// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function L = linalg_cholnaive(A)
    // Returns the Cholesky decomposition A = C*C'

    [n n] = size(A);
    for j = 1:n
        for k = 1:j-1
            A(j:n, j) = A(j:n, j) - A(j:n, k)*A(j, k);
        end
        A(j, j) = sqrt(A(j, j));
        A(j+1:n, j) = A(j+1:n, j) / A(j, j);
    end
    L = tril(A);
endfunction

C = [1 0 0 0
     1 2 0 0
     1 2 3 0
     1 2 3 4];
A = C * C';
B = linalg_cholnaive(A);
abserror = abs(B-C);
B = linalg_chol(A);
abserror = abs(B-C);

// Load this script into the editor
filename = "exercise_chol.sce";
dname = get_absolute_file_path(filename);
editor(fullfile(dname,filename));

