// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - Sylvan Brocard
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function rtol = demo_zhbev ( n , nsubdiag )
    //
    // Returns the relative error for zhbev compared to spec
    // for a particular n-by-n Hermitian Band matrix with
    // nsubdiag sub-diagonals.

    Av = 0;
    Sd = 1;
    Ar = zeros(n,n);
    for i = 1 : n
        j = (i:i+nsubdiag-1);
        j = max(min(j,n),1);
        Ar(i,j) = grand(1,nsubdiag,"nor",Av,Sd);
    end
    Ai = zeros(n,n);
    for i = 1 : n
        j = (i:i+nsubdiag-1);
        j = max(min(j,n),1);
        Ai(i,j) = grand(1,nsubdiag,"nor",Av,Sd);
    end
    Afull = (Ar+%i*Ai) + (Ar'-%i*Ai');
    // Convert it into band-form
    AHLband=linalg_hbandL(Afull,nsubdiag);
    // Use zhbev
    D = linalg_zhbev ( AHLband );
    D = gsort(D,"g","i");
    // Compare with spec
    Dfull = spec(Afull);
    Dfull = gsort(Dfull,"g","i");
    // Display relative error : rtol should be from 10 to 1000
    rtol = max(abs(D-Dfull)./abs(D))
endfunction


function rtol = demo_dgemm ( m , n , k )
  // Returns the relative error for a particular call to
  // dgemm.
  Av = 0;
  Sd = 1;
  Alpha = 2;
  Beta = 3;
  A = grand(m,k,"nor",Av,Sd);
  B = grand(k,n,"nor",Av,Sd);
  C = grand(m,n,"nor",Av,Sd);
  D = linalg_dgemm(Alpha, A, B, Beta, C)
  // Compare with Scilab's:
  Dsci = Alpha*A*B+ Beta*C
  rtol = norm(D-Dsci,"inf")/norm(Dsci,"inf")
endfunction

function rtol = demo_zgemm ( m , n , k )
  // Returns the relative error for a particular call to
  // zgemm.
  Av = 0;
  Sd = 1;
  Alpha = 1 + %i;
  Beta = Alpha;
  A = grand(m,k,"nor",Av,Sd) + %i * grand(m,k,"nor",Av,Sd);
  B = grand(k,n,"nor",Av,Sd) + %i * grand(k,n,"nor",Av,Sd);
  C = grand(m,n,"nor",Av,Sd) + %i * grand(m,n,"nor",Av,Sd);
  D = linalg_zgemm(Alpha, A, B, Beta, C)
  // Compare with Scilab's:
  Dsci = Alpha*A*B+ Beta*C
  rtol = norm(D-Dsci,"inf")/norm(Dsci,"inf")
endfunction

stacksize("max");
// Size of the matrix
n = 1000;
// Number of loops
nloops = 10;

mprintf("Search for memory leaks in zhbev...\n")
nsubdiag = 5;
for k = 1:  nloops
  rtol = demo_zhbev ( n )/%eps;
  mprintf("Run #%d: rtol = %f\n",k,rtol)
end

mprintf("Search for memory leaks in dgemm...\n")
m = n;
k = n;
for k = 1:  nloops
  rtol = demo_dgemm ( m , n , k )/%eps;
  mprintf("Run #%d: rtol = %f\n",k,rtol)
end

mprintf("Search for memory leaks in zgemm...\n")
m = n;
k = n;
for k = 1:  nloops
  rtol = demo_zgemm ( m , n , k )/%eps;
  mprintf("Run #%d: rtol = %f\n",k,rtol)
end

