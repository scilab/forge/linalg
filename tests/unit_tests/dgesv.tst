// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

// One RHS in the linear system of equations
A=  [
1 2
3 4
];
e = [5;6];
b = [17;39];
x = linalg_dgesv(A,b);
assert_checkalmostequal(x,e);
//
// 3 RHS in the linear system of equations
A=  [
1 2
3 4
];
e = [5,1,2,3;6,2,3,4];
b = [17,5,8,11;39,11,18,25];
x = linalg_dgesv(A,b);
assert_checkalmostequal(x,e);
// A singular system
A=  [
1 2
1 2
];
b = [-66;-74];
instr = "x = linalg_dgesv(A,b)";
localmsg = msprintf(gettext("%s: Matrix is singular."),"linalg_dgesv");
assert_checkerror(instr,localmsg);


