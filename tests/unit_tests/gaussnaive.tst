// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->



// Test gauss naive
A = [
3 17 10
2 4 -2
6 18 -12
];
b = [
67
4
6
];
x = linalg_gaussnaive ( A , b ) ;
xe = [1 2 3]';
assert_checkalmostequal ( x , xe);
// See what happens
x = linalg_gaussnaive ( A , b , %t ) ;

// Test difficult case
A = [
2 * %eps 1
1     1
];
xe = [1/10 1/3]'
b = A * xe;
x = linalg_gaussnaive ( A , b , %f ) ;
// Very wrong...
assert_checkalmostequal ( x , xe , 1. );

