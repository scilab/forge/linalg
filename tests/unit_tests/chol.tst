// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->



//
C = [
1 0 0 0
1 2 0 0
1 2 3 0
1 2 3 4
];
A = C*C';
B=linalg_chol(A);
assert_checkequal ( B , C );
//
// Source: https://ece.uwaterloo.ca/~dwharder/NumericalAnalysis/04LinearAlgebra/cholesky/
L2 = [
2.236067977499790   0.000000000000000   0.000000000000000   0.000000000000000
0.536656314599949   2.389979079406345   0.000000000000000   0.000000000000000
0.134164078649987  -0.197491268466351   2.818332343581848   0.000000000000000
-0.268328157299975   0.436823907370487   0.646577012719190   3.052723872310221
];
A = [
5 1.2 0.3 -0.6;
1.2 6 -0.4 0.9;
0.3 -0.4 8 1.7;
-0.6 0.9 1.7 10
];
L = linalg_chol(A);
assert_checkalmostequal(L,L2,100*%eps,[],"element");
//
A = [
0.9 0.06 -0.39 -0.24;
0.06 1.604 0.134 0.464;
-0.39 0.134 2.685 0.802;
-0.24 0.464 0.802 1.977
];
L = linalg_chol( A );
b = [0.063, -0.6358, 0.5937, -0.1907]';
y = L \ b;
x = L' \ y;
e = [0.2; -0.4; 0.3; -0.1];
assert_checkalmostequal(x,e,100*%eps,[],"element");

